import { createApp} from 'vue'
import TestJWT from './TestJWT.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import axios from "axios"
import VueAxios from "vue-axios"
import VueCookies from "vue-cookies"

axios.defaults.baseURL = "http://localhost:5000"

createApp(TestJWT).use(VueCookies).use(VueAxios, axios).mount('#app')