from flask import Flask, jsonify
from flask_cors import CORS
from flask_jwt_extended import create_access_token, JWTManager, jwt_required

DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

app.config["JWT_SECRET_KEY"] = "arvid"
JWTManager(app)

CORS(app)

#Public routes

@app.route("/getToken", methods=["GET"])
def getToken():
    token = create_access_token(identity="token1")
    return jsonify(token)

@app.route("/nonLogged", methods=["GET"])
def NonLogged():
    
    return jsonify("Accès route sans authentication réussi")

#Private Routes

@app.route("/logged", methods=["GET"])
@jwt_required()
def Logged():
    return jsonify("Accès route avec authentification")

if __name__ == '__main__':
    app.run()
